import { CommonService } from '../../provider/Common/common.service';
import { Group } from '../../model/group-model';
import { Component, OnInit } from '@angular/core';
import { NavParams, NavController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-groupdetail',
  templateUrl: './groupdetail.page.html',
  styleUrls: ['./groupdetail.page.scss'],
})
export class GroupdetailPage implements OnInit {

  group:Group = null;
  isJoined=false;

  ngOnInit() {
  
  }
  onJoined(){
    this.isJoined = true;
  this.navctrl.back();
  }
  constructor(
    private navctrl:NavController,
    private commonsrnc:CommonService,
    private route: ActivatedRoute, 
    private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (params && params.group) {
        this.group = JSON.parse(params.group);
      }
    });
  }

}
