import { NavController, LoadingController } from '@ionic/angular';
import { CommonService } from '../../provider/Common/common.service';
import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/model/image-model';
import { HttpClient } from '@angular/common/http';
import { AngularFireStorage } from '@angular/fire/storage';
 

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.page.html',
  styleUrls: ['./add-post.page.scss'],
})
export class AddPostPage implements OnInit {
  
  uploadedFileType: string = '';
  public obj = {
    date: '2020-12-25', 
    description:"",
    location:"",
    tags:"",
    imageurl:"",
    event:""
  }
  fileItem: any;
   
  constructor(private commnSrvc:CommonService,
    private http:HttpClient,
    private navctrl:NavController, 
    private store:AngularFireStorage,
    private loadingCtrl:LoadingController) {


   }

  ngOnInit() {
  
  }
  AddPost(){
    this.loadingCtrl
    .create({ keyboardClose: true, message: 'Uploading...' })
    .then(loadingEl => {
      loadingEl.present();
      this.uploadFileToFirebase(Date.now()+"",this.obj.imageurl)
    .then((fileurl:string)=>{
          debugger;
          this.obj.imageurl = fileurl;
          this.uploadToFirebaseDB(new Post(this.obj))
          .then(data=>{
            console.log(data);
            this.commnSrvc.fetchPost(()=>{
              loadingEl.dismiss();
              this.navctrl.back();
      
            })
           
          }).catch(ex=>{
            console.log(ex);
            loadingEl.dismiss();
            this.navctrl.back();
          
          });
        }).catch(ex=>{
          loadingEl.dismiss();
          this.navctrl.back();
        });
      });
 
  }

 
    uploadToFirebaseDB(newpost:Post){
      return this.http.post(`${this.commnSrvc.baseurlFirebase}postimage.json`,
      {...newpost}).toPromise();
      
    }

   uploadFileToFirebase(id,file){
    return new Promise( async(resolve,reject)=>{

      console.log("file",file);
      if(file ){
   
        const task = await this.store.ref('images').child(id).put(file);
       this.store.ref(`images/${id}`).getDownloadURL().toPromise()
       .then(x=>resolve(x)).catch(x=>reject(x));
      }
      else
      {
        reject("File Error")
      }
    })
      
      
    }


  InvokeuploadFile(){
    
    console.log("fileitem:::",this.fileItem);
    this.fileItem = null;
    console.log("fileitem:::",this.fileItem);
    // setTimeout(() => {
      document.getElementById('uploadPost').click();  
    // }, 100,this);
    
  }
  
  selectedPostImage(event){
    
    this.uploadedFileType = event.target.files[0].type;
    console.log("EVENT:::",event.target.files[0].type);
    debugger;
    if(event.target.files[0].size > 10000000){
      console.log("Size is too big");
      
    }else{
      if(event.target.files && event.target.files[0]){
      //   var reader = new FileReader();
      setTimeout(() => {
        if(this.uploadedFileType == 'image/jpeg' || this.uploadedFileType == 'image/jpg' || this.uploadedFileType == 'image/png'){
          let imagePanel = document.getElementById('uploadedImage');
          console.log("IMAGE - PANEL",imagePanel);
          imagePanel['src'] =  URL.createObjectURL(event.target.files[0]);
       
          this.obj. imageurl =  event.target.files[0] ;
        } 
      }, 0),this;
        
 
      
        }
      }
       
    }

}
