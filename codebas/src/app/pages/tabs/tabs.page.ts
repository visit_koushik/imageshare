import { CommonService } from './../../provider/Common/common.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private com:CommonService) {

    this.com.fetchUserinfo();
  }

}
