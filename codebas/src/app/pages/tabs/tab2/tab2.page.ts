import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { Component,Pipe } from '@angular/core';
import { CommonService } from '../../../provider/Common/common.service';
 

 

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor(private commonsrvc: CommonService,
    private  navCtrl:NavController,
    private http: HttpClient) {}

    ionViewWillEnter(){
      debugger
      this.commonsrvc.fetchPost();
    }
  onAddPost(){
    this.navCtrl.navigateForward("add-post");
  }

}
