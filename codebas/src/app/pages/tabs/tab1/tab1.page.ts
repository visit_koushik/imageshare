 
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
import { CommonService } from '../../../provider/Common/common.service'; 
import { Component, OnInit } from '@angular/core';  

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{

 

  constructor(private commonsrvc: CommonService,
   private  navCtrl:NavController,
   private http: HttpClient) {
 
    
  }
  ngOnInit(): void {
     
  }

 
ionViewWillEnter(){
  debugger;
    this.commonsrvc.fetchGroup();
  }
  
  onCreateGroup(){
    this.navCtrl.navigateForward('create-group');
  }


}
