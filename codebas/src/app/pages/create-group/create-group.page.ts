import { HttpClient } from '@angular/common/http';
import { Group } from '../../model/group-model';
import { CommonService } from '../../provider/Common/common.service';
import { NavController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core'; 

@Component({
  selector: 'app-create-group',
  templateUrl: './create-group.page.html',
  styleUrls: ['./create-group.page.scss'],
})


export class CreateGroupPage implements OnInit {
  usrname:string="";
  grpName:string="";


  constructor(private navctrl:NavController,
    private commonsrvc:CommonService,
    private http:HttpClient) { }

  ngOnInit() {
  }
  creategrp(){
    console.log(this.usrname +"   "+ this.grpName);
    this. uploadToFirebaseDB(new Group(this.grpName,this.usrname)).
    then(x=>{
      this.commonsrvc.fetchGroup(()=>{
        this.navctrl.back();

      })
    }).catch(ex=>{
      this.navctrl.back();
    })
   
  }

  uploadToFirebaseDB(newgroup:Group){
    return this.http.post(`${this.commonsrvc.baseurlFirebase}postGroup.json`,
    {...newgroup}).toPromise();
 
  }
  
}
