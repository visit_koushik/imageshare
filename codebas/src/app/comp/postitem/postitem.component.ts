import { Post } from 'src/app/model/image-model';
import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-postitem',
  templateUrl: './postitem.component.html',
  styleUrls: ['./postitem.component.scss'],
})
export class PostitemComponent implements OnInit {

  @Input() post:Post;

  constructor(private sanitized: DomSanitizer) { }

  ngOnInit() {}

  getUrl(){
 
    return this.sanitized.bypassSecurityTrustResourceUrl(this.post?.imageurl);
  }
}
