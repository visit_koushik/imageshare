import { Component, Input, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Group } from '../../model/group-model';

@Component({
  selector: 'app-grp',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss'],
})
export class  GroupComponent implements OnInit {

  @Input() group: Group;
  constructor(private router:Router) { }

  onGroupDetail( ){
    console.log(this.group);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        group: JSON.stringify(this.group)
      }
    };
    this.router.navigate(['groupdetail'], navigationExtras);
  }
  ngOnInit() {}

}
