export class User {

    public id: string="";
    public email: string="";
    private _token: string="";
    private tokenExpirationDate: Date=null;
    constructor(   id: string,
          email: string,
          token: string="",
          tokenExpirationDate: Date=null
     
    ) {

        this.id=id;
        this.email = email;
        this._token = token;
        this.tokenExpirationDate = tokenExpirationDate;
    }
     
    get token() {
      if (!this.tokenExpirationDate || this.tokenExpirationDate <= new Date()) {
        return null;
      }
      return this._token;
    }
  }
  