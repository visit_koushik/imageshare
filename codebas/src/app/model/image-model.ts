export class Post{
    public description="";
    public tags = "";
    public date = "";
    public imageurl = "";
    public location = "";
    public event = "";
    public id="";

    constructor(obj,id="" ){
         this.description = obj.description;
         this.tags = obj.tags;
         this.date = obj.date;
         this.imageurl = obj.imageurl;
         this.location = obj.location;
         this.event = obj.event;
         this.id=id;
    }
}