import { User } from './../../model/user-model';
import { Plugins } from '@capacitor/core';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Group } from 'src/app/model/group-model';
import { Post } from 'src/app/model/image-model';

 
@Injectable({
  providedIn: 'root'
})
export class CommonService {

  groups=[];
  posts=[];

  baseurlFirebase = "https://imageupload-3b13a-default-rtdb.firebaseio.com/";
  users: any[];
  constructor(private http:HttpClient) { }

  fetchGroup(callbak=null){
    this.http.get
    (`${this.baseurlFirebase}postGroup.json`)
    .toPromise().then(resdata=>{
      const gdata = [];
      for(const key in resdata){
        gdata.push(new Group(resdata[key].name,resdata[key].created,key));
      }
      this.groups = [...gdata];
      if(callbak)
      {
        callbak();
      }
    }).catch(ex=>{
      if(callbak)
      {
        callbak();
      }
    });
     

    
  }


  fetchPost(callbak=null){
    this.http.get 
    (`${this.baseurlFirebase}postimage.json`)
    .toPromise()
    .then(resdata=>{
      const gdata = [];
      for(const key in resdata){
        gdata.push(new Post(resdata[key],key));
      }
      
      this.getUserInfo().then(e=>{
        let u = JSON.parse(e.value) as {
          token: string;
          tokenExpirationDate: string;
          userId: string;
          email: string;
        };

        this.posts = gdata.filter((r:Post)=>{
          if(r.tags?.indexOf(u.email)>=0){
            return r;
          }
        })
      })
      if(callbak)
      {
        callbak();
      }
      return gdata;
    })
    .catch(ex=>{
      if(callbak)
      {
        callbak();
      }
    })
    
  }

  //I know it is not good practice
  fetchUserinfo(callbak=null){
    this.http.get 
    (`${this.baseurlFirebase}userinfo.json`)
    .toPromise()
    .then(resdata=>{
      const gdata = [];
      // tslint:disable-next-line:forin
      for(const key in resdata){
        gdata.push(new User(key,resdata[key].email));
      }
      this.users = [...gdata];

      if(callbak)
      {
        callbak();
      }
      return gdata;
    })
    .catch(ex=>{
      if(callbak)
      {
        callbak();
      }
    })
    
  }

  getUserInfo(){
   return Plugins.Storage.get({ key: 'authData'});
  }

  remove(){
    Plugins.Storage.clear();
  }
}
