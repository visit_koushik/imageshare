import { PostitemComponent } from './app/comp/postitem/postitem.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core'; 
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular'; 
import { GroupComponent } from './app/comp/groupitem/group.component';
@NgModule({
  declarations: [
    GroupComponent,
    PostitemComponent
  ],
  imports: [
    CommonModule,
    IonicModule, 
    FormsModule
  ],
  exports: [
    GroupComponent,
    PostitemComponent
  ]
})
export class SharedComponentsModule { }
