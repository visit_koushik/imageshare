// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
        apiKey: "AIzaSyCRXh3fncRbhuwfizwXrNRsDqBtNwCn8Gg",
        authDomain: "imageupload-3b13a.firebaseapp.com",
        projectId: "imageupload-3b13a",
        storageBucket: "imageupload-3b13a.appspot.com",
        messagingSenderId: "781808843380",
        appId: "1:781808843380:web:af574f1443f8f2664b2f21",
        measurementId: "G-CSHF73J0Y6"
      }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
